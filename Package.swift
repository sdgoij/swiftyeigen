// swift-tools-version:5.5
import PackageDescription

let package = Package(
    name: "Eigen",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "ObjCEigen",
            targets: ["ObjCEigen"]
        ),
        .library(
            name: "SwiftyEigen",
            targets: ["SwiftyEigen"]
        ),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "ObjCEigen",
            path: "Sources/ObjC",
            cxxSettings: [
                .headerSearchPath("../CPP/"),
                .define("EIGEN_MPL2_ONLY")
            ]
        ),
        .target(
            name: "SwiftyEigen",
            dependencies: ["ObjCEigen"],
            path: "Sources/Swift"
        ),
    ]
)
